<?php
$pageTitle="Common Problems - North Florida Home Inspection";
include 'header.php';
?>

	<div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">
        
        <h1>Common Problems</h1>
        <p>No house is perfect. Even the best built and best maintained homes will always have a few items in less than perfect condition.  
			Below are some of the items we most commonly find when inspecting a home.</p>
      </div>
    </div>

    <div class="container well">
        <div class="fluid_container">

			<h3>Structural</h3>
			<br/>
			<p>Cut trusses most commonly occurs when air conditioning units are located in the attic.  Installers will cut away sections of 
			trusses to make installation of the unit easier or to make space for the unit to fit.  Trusses are engineered components, built with specific 
			weight loads. Removing any portion of the truss can weaken the roof structure and should not be done without an engineers’ approval and the 
			addition of prescribed support.
			</p>
			<p>
			Moisture damaged sheathing, due to roof leaks or roof penetration flashing leaks. Old roof coverings and deteriorated flashing at skylights, 
			chimneys and plumbing stacks are the most common causes.  Other, less obvious, causes are improperly installed flashing or flashing that is 
			missing entirely. This is most common at the roof to wall connections, where the wall continues on past the edge of the roof.  Missing kick-out 
			flashings allow water to penetrate into the wall, causing rot and mold on the interior of the house. 
			</p>
			
			<div class="carousel slide" data-ride="carousel" id="attic">
			<!-- ATTIC -->
			<ol class="carousel-indicators">
				<li data-target="#attic" data-slide-to="0" class="active"></li>
				<li data-target="#attic" data-slide-to="1"></li>
				<li data-target="#attic" data-slide-to="2"></li>
			</ol>
 

			<div class="carousel-inner">
				<div class="item active">
					<img src="images/problems/cut_truss_ac.png" alt="...">
					<div class="carousel-caption">
						<h3>Cut truss to allow AC installation</h3>
					</div>
			</div>
			<div class="item">
				<img src="images/problems/flash_leaking.png" alt="...">
				<div class="carousel-caption">
					<h3>Home owner attempt to hide flashing leak, plastic sheeting</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/moist_damage_chim.png" alt="...">
				<div class="carousel-caption">
					<h3>Moisture damage from leaking chimney</h3>
				</div>
			</div>
		</div>
 
		<!-- Controls -->
		<a class="right carousel-control" href="#attic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
		<a class="left carousel-control" href="#attic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		</div> <!-- Carousel -->
			
			<h3>Roofing</h3>
			<p>
			Problems with roofing materials are the single most common defect we find.  Usually, it does not mean the roof needs to be replaced simply 
			that it is need of maintenance or repair.
			</p>
			
			
			<div class="carousel slide" data-ride="carousel" id="roof">
			<!-- ROOF -->
			<ol class="carousel-indicators">
				<li data-target="#roof" data-slide-to="0" class="active"></li>
				<li data-target="#roof" data-slide-to="1"></li>
				<li data-target="#roof" data-slide-to="2"></li>
				<li data-target="#roof" data-slide-to="3"></li>
				<li data-target="#roof" data-slide-to="4"></li>
				<li data-target="#roof" data-slide-to="5"></li>
				<li data-target="#roof" data-slide-to="6"></li>
				<li data-target="#roof" data-slide-to="7"></li>
			</ol>
 
  
		<div class="carousel-inner">
			<div class="item active">
			<img src="images/problems/roof_boot.png" alt="...">
				<div class="carousel-caption">
					<h3>Deteriorated plumbing boot, grit loss on shingles</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/roof_chim_gaps.png" alt="...">
				<div class="carousel-caption">
					<h3>Gaps in chimney flashing</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/roof_miss_shing.png" alt="...">
				<div class="carousel-caption">
					<h3>Damaged or missing shingles</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/roof_nail.png" alt="...">
				<div class="carousel-caption">
					<h3>Unsealed nail protruding through shingles, grit loss</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/roof_unsealed_nails.png" alt="...">
				<div class="carousel-caption">
					<h3>Unsealed nails at plumbing stack</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/roof_water_kickout1.png" alt="...">
				<div class="carousel-caption">
					<h3>Moisture damage from leaking chimney</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/roof_water_kickout2.png" alt="...">
				<div class="carousel-caption">
					<h3>Moisture damage from leaking chimney</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/moist_damage_chim.png" alt="...">
				<div class="carousel-caption">
					<h3>Moisture damage from leaking chimney</h3>
				</div>
			</div>
		</div>
 
		<!-- Controls -->
		<a class="left carousel-control" href="#roof" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#roof" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
		</div> <!-- Carousel -->


			<h3>Ceiling</h3>
			<p>
			Caused by past or present leaks, ceiling stains are very common.  It can be difficult to tell whether the stains 
			are from active leaks or were caused by old repaired leaks and never painted over.  
			</p>	
	
			<div class="carousel slide" data-ride="carousel" id="ceiling">
			<!-- CEILING -->
			<ol class="carousel-indicators">
				<li data-target="#ceiling" data-slide-to="0" class="active"></li>
				<li data-target="#ceiling" data-slide-to="1"></li>
				<li data-target="#ceiling" data-slide-to="2"></li>
				<li data-target="#ceiling" data-slide-to="3"></li>

			</ol>
 
  
		<div class="carousel-inner">
			<div class="item active">
			<img src="images/problems/ceiling_1.png" alt="...">
				<div class="carousel-caption">
					
				</div>
			</div>
			<div class="item">
				<img src="images/problems/ceiling_2.png" alt="...">
				<div class="carousel-caption">
					
				</div>
			</div>
			<div class="item">
				<img src="images/problems/ceiling_3.png" alt="...">
				<div class="carousel-caption">
					
				</div>
			</div>
			<div class="item">
				<img src="images/problems/ceiling_4.png" alt="...">
				<div class="carousel-caption">
					
				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#ceiling" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#ceiling" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
		</div> <!-- Carousel -->
		
			<h3>Electrical Hazards</h3>
			<p>
			Most common in older homes, but often found in newer homes as well.  Electrical hazards are common when wiring has been done incorrectly by the home owner.
			</p>	
	
			<div class="carousel slide" data-ride="carousel" id="electric">
			<!-- ELECTRIC -->
			<ol class="carousel-indicators">
				<li data-target="#electric" data-slide-to="0" class="active"></li>
				<li data-target="#electric" data-slide-to="1"></li>
				<li data-target="#electric" data-slide-to="2"></li>
				<li data-target="#electric" data-slide-to="3"></li>

			</ol>
 
  
		<div class="carousel-inner">
			<div class="item active">
			<img src="images/problems/elec_improper.png" alt="...">
				<div class="carousel-caption">
					<h3>Improper electrical connections in attic</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/elec_partial_upgrade.png" alt="...">
				<div class="carousel-caption">
					<h3>Partial wiring upgrade, outdated wires still in use</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/elec_two_wires.png" alt="...">
				<div class="carousel-caption">
					<h3>Double taps (two wires in one breaker)</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/elec_under_sized.png" alt="...">
				<div class="carousel-caption">
					<h3>Under-sized wiring</h3>
				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#electric" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#electric" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
		</div> <!-- Carousel -->

			<h3>Rotted Wood</h3>
			<p>
			Caused by being wet for extended periods of time, most commonly found around tubs, showers and toilets 
			on the interior, roof eaves and trim areas on the exterior.
			</p>	
	
			<div class="carousel slide" data-ride="carousel" id="rot">
			<!-- ROTTED -->
			<ol class="carousel-indicators">
				<li data-target="#rot" data-slide-to="0" class="active"></li>
				<li data-target="#rot" data-slide-to="1"></li>
				<li data-target="#rot" data-slide-to="2"></li>
				<li data-target="#rot" data-slide-to="3"></li>

			</ol>
 
  
		<div class="carousel-inner">
			<div class="item active">
			<img src="images/problems/rot_frame.png" alt="...">
				<div class="carousel-caption">
					<h3>Wood rot in door frame</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/rot_rail.png" alt="...">
				<div class="carousel-caption">
					<h3>Wood rot in railing</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/rot_siding.png" alt="...">
				<div class="carousel-caption">
					<h3>Wood rot in siding</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/rot_window.png" alt="...">
				<div class="carousel-caption">
					<h3>Wood rot in siding and window</h3>
				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#rot" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#rot" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
		</div> <!-- Carousel -->
		

			<h3>Water Heater Installations</h3>
			<p>
			Many water heaters are not installed in full compliance with local plumbing codes.
			</p>	
	
			<div class="carousel slide" data-ride="carousel" id="water">
			<!-- ROTTED -->
			<ol class="carousel-indicators">
				<li data-target="#water" data-slide-to="0" class="active"></li>
				<li data-target="#water" data-slide-to="1"></li>
				
			</ol>
   
		<div class="carousel-inner">
			<div class="item active">
			<img src="images/problems/water_tpr.png" alt="...">
				<div class="carousel-caption">
					<h3>Missing Temperature Pressure Relief (TPR) Valve</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/water_pan.png" alt="...">
				<div class="carousel-caption">
					<h3>Missing water pan under unit</h3>
				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#water" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#water" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
		</div> <!-- Carousel -->
		
			<h3>Plumbing Defects</h3>
			<p>
			Plumbing issues commonly found include leaking pipes, dripping faucets, leaking fixtures, 
			slow drains, loose toilets, etc.  It is common to identify minor plumbing defects in new 
			homes, such as reversed hot and cold supply lines to fixtures.
			</p>	
	
			<div class="carousel slide" data-ride="carousel" id="plumbing">
			<!-- ROTTED -->
			<ol class="carousel-indicators">
				<li data-target="#plumbing" data-slide-to="0" class="active"></li>
				<li data-target="#plumbing" data-slide-to="1"></li>
				<li data-target="#plumbing" data-slide-to="2"></li>
				<li data-target="#plumbing" data-slide-to="3"></li>
			</ol>
   
		<div class="carousel-inner">
			<div class="item active">
			<img src="images/problems/plumb_bathtub.png" alt="...">
				<div class="carousel-caption">
					<h3>Leak under bathtub in crawlspace </h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/plumb_rusted.png" alt="...">
				<div class="carousel-caption">
					<h3>Rusted, leaky pipes</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/plumb_toilet.png" alt="...">
				<div class="carousel-caption">
					<h3>Toilet leaking at the base</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/plumb_poly.png" alt="...">
				<div class="carousel-caption">
					<h3>Polybutelene piping may make the house uninsurable</h3>
				</div>
			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#plumbing" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#plumbing" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
		</div> <!-- Carousel -->

		
			<h3>Air Conditioning</h3>
			<p>
			Many air conditioners are in need of a new filter, coil cleaning and replacing the insulation 
			on the refrigerant lines at the exterior unit.  Ductwork is often damaged or in need of cleaning.
			</p>	
	
			<div class="carousel slide" data-ride="carousel" id="ac">
			<!-- ROTTED -->
			<ol class="carousel-indicators">
				<li data-target="#ac" data-slide-to="0" class="active"></li>
				<li data-target="#ac" data-slide-to="1"></li>
				
			</ol>
   
		<div class="carousel-inner">
			<div class="item active">
			<img src="images/problems/ac_disc.png" alt="...">
				<div class="carousel-caption">
					<h3>Disconnected branch duct in crawlspace</h3>
				</div>
			</div>
			<div class="item">
				<img src="images/problems/ac_damage.png" alt="...">
				<div class="carousel-caption">
					<h3>Vermin damage to duct in crawlspace </h3>
				</div>
			</div>
		</div>
	
		<!-- Controls -->
		<a class="left carousel-control" href="#ac" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#ac" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
		</div> <!-- Carousel -->

			
			<h3>Gas Furnace</h3>
			<p>
			Most gas furnaces seem to be in need of routine maintenance, such as new filters or gas company
			certifications.  Many have other issues such as faulty operation or inadequate clearance from combustible materials.
			</p>	
		
		<div class="carousel slide" data-ride="carousel" id="gas">
			<!-- GAS -->
			<ol class="carousel-indicators">
				<li data-target="#gas" data-slide-to="0" class="active"></li>
			</ol>
   
		<div class="carousel-inner">
			<div class="item active">
			<img src="images/problems/gas_fumes.png" alt="...">
				<div class="carousel-caption">
					<h3>No vertical rise in flue can cause fumes in house</h3>
				</div>
			</div>
		</div>
	
		<!-- Controls -->
		<a class="left carousel-control" href="#gas" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#gas" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
		</div> <!-- Carousel -->
			
			
			
		</div><!-- .fluid_container -->
    </div>

    
<?php
include 'footer.php';
?>