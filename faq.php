<?php
$pageTitle="FAQ - North Florida Home Inspection";
include 'header.php';
?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">
       
        <h1>FAQ</h1>
        <p>Take a look at some of our frequently asked questions.</p>
      </div>
    </div>

    <div class="container well">
        <h3>What is a home inspection?</h3>
<p>A home inspection is an objective visual examination of the physical structure and systems of a house, from the roof to the foundation.
        </p>


        <h3>What does a home inspection include?</h3>
<p>The standard home inspector’s report will cover the condition of the home’s heating system; central air conditioning system (temperature permitting); interior plumbing and electrical systems; the roof, attic and visible insulation; walls, ceilings, floors, windows and doors; the foundation, basement and structural components.
        </p>

        <h3>Why do I need a home inspection?</h3>
<p>Buying a home could be the largest single investment you will ever make. To minimize unpleasant surprises and unexpected difficulties, you’ll want to learn as much as you can about the newly constructed or existing house before you buy it. A home inspection may identify the need for major repairs or builder oversights, as well as the need for maintenance to keep it in good shape. After the inspection, you will know more about the house, which will allow you to make decisions with confidence.

If you already are a homeowner, a home inspection can identify problems in the making and suggest preventive measures that might help you avoid costly future repairs.

If you are planning to sell your home, a home inspection can give you the opportunity to make repairs that will put the house in better selling condition.
        </p>


        <h3>What will it cost?</h3>
<p>The inspection fee for a typical one-family house varies geographically, as does the cost of housing. Similarly, within a given area, the inspection fee may vary depending on a number of factors such as the size of the house, its age and possible optional services such as septic, well or radon testing.

Do not let cost be a factor in deciding whether or not to have a home inspection or in the selection of your home inspector. The sense of security and knowledge gained from an inspection is well worth the cost, and the lowest-priced inspection is not necessarily a bargain. Use the inspector’s qualifications, including experience, training, compliance with your state’s regulations, if any, and professional affiliations as a guide.
        </p>


        <h3>Why can't I do it myself?</h3>
<p>Even the most experienced homeowner lacks the knowledge and expertise of a professional home inspector. An inspector is familiar with the elements of home construction, proper installation, maintenance and home safety. He or she knows how the home’s systems and components are intended to function together, as well as why they fail.

Above all, most buyers find it difficult to remain completely objective and unemotional about the house they really want, and this may have an effect on their judgment. For accurate information, it is best to obtain an impartial, third-party opinion by a professional in the field of home inspection.
        </p>


        <h3>Can a house fail a home inspection?</h3>
<p>No. A professional home inspection is an examination of the current condition of a house. It is not an appraisal, which determines market value. It is not a municipal inspection, which verifies local code compliance. A home inspector, therefore, will not pass or fail a house, but rather describe its physical condition and indicate what components and systems may need major repair or replacement.
        </p>


        <h3>How do I find a home inspector?</h3>
<p>You can ask friends or business acquaintances to recommend a home inspector they have used. Or, you can use the Find An Inspector search tool for a list of home inspectors in your area who belong to the non-profit professional organization. To have a list mailed to you, call 1-800-743-ASHI (2744). Also, real estate agents and brokers are familiar with the service and may be able to provide you with a list of names from which to choose.

Whatever your referral source, you can be assured of your home inspector’s commitment to professional standards and business ethics by choosing one who has membership in ASHI.
        </p> 
        
        <p>&copy; American Society of Home Inspectors ®, Inc. </p>
</div>
<?php
include 'footer.php';
?>