<?php
$pageTitle="Sample Reports - North Florida Home Inspection";
include 'header.php';
?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">
        
        <h1>Sample Reports</h1>
        <p>See what a report generally looks like.</p>
      </div>
    </div>

    <div class="container well img-responsive text-center">
        <h2>Sample 1:</h2>
        <iframe src="http://docs.google.com/gview?url=http://michaelhelfrich.com/LIS4381/p2/global/report1.pdf&amp;embedded=true" style="width:75%; height:500px;"></iframe>
        <br />
        <br />
        <h2>Sample 2:</h2>
        <iframe src="http://docs.google.com/gview?url=http://michaelhelfrich.com/LIS4381/p2/global/report2.pdf&amp;embedded=true" style="width:75%; height:500px;"></iframe>
    </div>
<?php
include 'footer.php';
?>