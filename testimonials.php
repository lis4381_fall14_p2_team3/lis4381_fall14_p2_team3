<?php
$pageTitle="Testimonials - North Florida Home Inspection";
include 'header.php';
?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">
       
        <h1>Testimonials</h1>
        <p>See what some of our customers are saying about us.</p>  
      </div>
    </div>

    <div class="container well">
      <div class="row">
       <div class="col-md-4">
          <h3>Fast and affordable!</h3>
          <p><strong>By: John Smith</strong></p>
          <p>NFHI was available the day after my first phone call. They worked with my schedule to arrive at the best possible time. The price was fair and reasonable. I'm beyond satisfied with the hard working people at North Florida Home Inspection.</p>
       </div>
       <div class="col-md-4">
          <h3>Satisfied!</h3>
          <p><strong>By: Jane Doe</strong></p>
          <p>I'm beyond satisfied with my experience from North Florida Home Inspection. They were knowledgable and helpful during the inspection on my home. They were extremely affordable and even made recommendations that were affordable and doable.</p>
       </div>
       <div class="col-md-4">
          <h3>Couldn't ask for better!</h3>
          <p><strong>By: Jeremy Winklemyer</strong></p>
          <p>Wow is all I can say. The people at North Florida Home Inspection were so professional and timely. They were quick and thorough in their inspection. They notified me of all the problems and explained them clearly to me. They even provided referrals to other companies to perform repairs. Thanks NFHI!</p>
       </div>
      </div>    
    </div>
<?php
include 'footer.php';
?>