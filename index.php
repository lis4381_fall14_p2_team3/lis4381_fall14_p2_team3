<?php
$pageTitle="North Florida Home Inspection";
include 'header.php';
?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">
       
        <h1>Why inspect?</h1>
        <p>It doesn’t matter if the house is new or old. Mistakes can happen in construction or when repairs are made. Building materials can be flawed and will deteriorate over time. Installation techniques may not be correct. Getting an inspection can identify these issues up front, lowering your risk.</p>
        <p><a class="btn btn-success btn-lg" href="about.php" role="button">Learn more &raquo;</a></p>
      </div>
    </div>

<div class="container well text-center img-responsive">
    
<img id="Image-Maps-Com-image-maps-2014-12-04-091802" class="img-responsive text-center" src="images/house.PNG" width="715" height="520" usemap="#image-maps-2014-12-04-091802" alt="clickable house" />
<map name="image-maps-2014-12-04-091802" id="image-maps-2014-12-04-091802">
<area  alt="" title="" href="images/flashing.jpg" shape="rect" coords="325,133,369,163" style="outline:none;" target="_blank"     />
<area  alt="" title="" href="images/kickout.jpg" shape="rect" coords="428,205,472,235" style="outline:none;" target="_blank"     />
<area  alt="" title="" href="images/rotted.JPG" shape="rect" coords="339,329,383,359" style="outline:none;" target="_blank"     />
<area  alt="" title="" href="images/beam.jpg" shape="rect" coords="90,215,134,245" style="outline:none;" target="_blank"     />
<area  alt="" title="" href="images/ridgevent.jpg" shape="rect" coords="177,147,221,177" style="outline:none;" target="_blank"     />
</map>

</div>

    <div class="container well">
      <div class="row">
        <div class="col-md-4">
          <h2>Location</h2>
          <p><strong>Address:</strong><br />North Florida Home Inspection<br />1503 14th Court<br />Lynn Haven, Florida</p>
        </div>
        <div class="col-md-4">
          <h2>Hours</h2>
          <p><strong>Monday - Saturday: </strong>9AM-5PM</p>
          <p><strong>Sunday: </strong>Closed</p>
       </div>
        <div class="col-md-4">
          <h2>Contact</h2>
          <p><strong>Phone: </strong>(850) 265-9006</p>
          <p>Matt Kelley - <a href="mailto:matt.nfhi@gmail.com">matt.nfhi@gmail.com</a></p>
          <p>Craig La Mere - <a href="mailto:craig.nfhi@gmail.com">craig.nfhi@gmail.com</a></p>
        </div>
      </div>
    </div>
<?php
include 'footer.php';
?>