<?php
$pageTitle="Contact Us - North Florida Home Inspection";
include 'header.php';
?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">
        
        <h1>Contact Us</h1>
        <p>Get in touch with us!</p>
      </div>
    </div>

    <div class="container table-responsive well">
        <p>Below is a contact form to reach us with a question you may have. You can also reach us by calling <strong>(850) 265-9006</strong> on the phone.</p>
        <p style="color: red;">The * indicates that a response is required!</p>
        <form name="contactform" method="post" action="contact_form_email.php">
            <table class="table table-bordered table-striped">

                <tr>
                 <td>
                  <label>First Name *</label>
                 </td>
                 <td>
                  <input type="text" name="first_name" maxlength="50" size="30" class="form-control input-md" required="">
                 </td>
                </tr>

                <tr>
                 <td>
                  <label>Last Name *</label>
                 </td>
                 <td>
                  <input type="text" name="last_name" maxlength="50" size="30" class="form-control input-md" required="">
                 </td>
                </tr>

                <tr>
                 <td>
                  <label>Email Address *</label>
                 </td>
                 <td>
                  <input type="text" name="email" maxlength="80" size="30" class="form-control input-md"  required="">
                 </td>
                </tr>

                <tr>
                 <td>
                  <label>Telephone Number</label>
                 </td>
                 <td>
                  <input type="text" name="telephone" maxlength="30">
                 </td>
                </tr>

                <tr>
                 <td>
                  <label>Your Message *</label>
                 </td>
                 <td>
                   <textarea name="comments" maxlength="500" class="form-control input-md"  required=""></textarea>
                  </td>
                </tr>

                <tr>
                  <td colspan="2" class="text-center">
                   <input class="btn btn-success" type="submit" value="Submit">
                  </td>
                </tr>
        
            </table>
        </form>
    </div>
<?php
include 'footer.php';
?>