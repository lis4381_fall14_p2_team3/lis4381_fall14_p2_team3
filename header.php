<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="North Florida Home Inspection">
    <meta name="author" content="Group 3">
    <link rel="icon" href="favicon.ico">

    <title><?php echo $pageTitle; ?></title>
      
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/theme.css" rel="stylesheet">

    <!-- Just for debugging purposes. -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body role="document">

    <!-- Fixed navbar -->
    <nav class="navbar navbar-fixed-top navbar-default navbar-default-border" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="index.php"><img src="images/logo_small.png" class="img-responsive" alt="Logo"/></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="text-strong"><a href="index.php">Home</a></li>
            <li class="text-strong"><a href="about.php">Why NFHI?</a></li>
            <li class="dropdown text-strong">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Who Needs An Inspection?<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li class="text-strong"><a href="whoneeds.php">Who Needs An Inspection?</a></li>
                <li class="divider"></li>
                <li class="text-strong"><a href="common.php">Common Problems</a></li>
                <li class="text-strong"><a href="mold.php">Mold Sampling</a></li>
				<li class="text-strong"><a href="sample.php">Sample Reports</a></li>
                <li class="text-strong"><a href="prices.php">Our Prices</a></li>
              </ul>
            </li>
            <li class="text-strong"><a href="contact.php">Contact</a></li>
            <li class="text-strong"><a href="faq.php">FAQ</a></li>
            <li class="text-strong"><a href="testimonials.php">Testimonials</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>