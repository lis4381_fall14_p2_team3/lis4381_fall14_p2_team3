    <div class="container">
      <hr />
      <div id="footer">
        <div class="container text-center">
            <h4>Members of:</h4>
           
               <a href="http://www.nachi.org/" target="_blank">
                <img src="images/nachi.png" alt="nachi" class="img-responsive image-block"/>
            </a>
                
              <a href="http://www.nahi.org/" target="_blank">
                <img src="images/nahi.png" alt="nahi" class="img-responsive image-block"/>
            </a>
                
         <a href="http://www.fabi.org/" target="_blank">
                <img src="images/fabi.png" alt="fabi" class="img-responsive image-block"/>
            </a>
      
            
            <p class="text-muted credit">&copy; North Florida Home Inspections 2014</p>
            <p>
                <a href="http://www.w3.org/html/logo/">
                    <img src="http://www.w3.org/html/logo/badge/html5-badge-h-css3-semantics.png" width="165" height="64" alt="HTML5 Powered with CSS3 / Styling, and Semantics" title="HTML5 Powered with CSS3 / Styling, and Semantics">
                </a>
            </p>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <script>
	
	jQuery(document).ready(function($){
	$('#attic').carousel();
	
	jQuery(document).ready(function($){
	$('#roof').carousel();
	
	jQuery(document).ready(function($){
	$('#ceilng').carousel();
	
	jQuery(document).ready(function($){
	$('#electric').carousel();
	
	jQuery(document).ready(function($){
	$('#rot').carousel();
	
	jQuery(document).ready(function($){
	$('#water').carousel();
	
	jQuery(document).ready(function($){
	$('#gas').carousel();
	
	jQuery(document).ready(function($){
	$('#ac').carousel();
	
	jQuery(document).ready(function($){
	$('#plumbing').carousel();
	</script>

  </body>
</html>