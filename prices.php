<?php
$pageTitle="Our Prices - North Florida Home Inspection";
include 'header.php';
?>


    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">
        
        <h1>Our Prices</h1>
        <p>We inspect small to large spaces. Let us know how we can help!</p>
      </div>
    </div>

    <div class="container well">

        <div class="row">
            <div class="col-md-4">

            <h2>Price Guide*</h2>
                <table>
                    <tr><td class="tdspecial">Up to 999 square feet</td> <td class="right">$220.00</td></tr>
                    <tr><td class="tdspecial">1000-1499  square feet</td> <td class="right">$240.00</td></tr>
                    <tr><td class="tdspecial">1500-1999  square feet</td> <td class="right">$260.00</td></tr>
                    <tr><td class="tdspecial">2000-2499  square feet</td> <td class="right">$280.00</td></tr>
                    <tr><td class="tdspecial">2500-2999  square feet</td> <td class="right">$320.00</td></tr>
                    <tr><td class="tdspecial">3000-3499  square feet</td> <td class="right">$370.00</td></tr>
                    <tr><td class="tdspecial">3500-3999  square feet</td> <td class="right">$420.00</td></tr>
                    <tr><td class="tdspecial">4000 and above</td> <td class="right">Call for quote</td></tr>
                    <tr><td class="tdspecial">Four-Point Insurance Letter</td> <td class="right">$125.00</td></tr>
                    <tr><td class="tdspecial">Commercial Properties</td><td class="right">Call for quote</td></tr>
                </table>
            </div>
            <div class="col-md-4">

            <h3>CONDOMINIUMS</h3>
                <table>
                    <tr><td class="tdspecial">Up to 1499 square feet</td><td class="right">$185.00</td></tr>
                    <tr><td class="tdspecial">1500-1999  square feet</td><td class="right">$210.00</td></tr>
                    <tr><td class="tdspecial">2000-2499  square feet</td><td class="right">$235.00</td></tr>
                </table>

            </div>
            <div class="col-md-4">

            <h3>ADDITIONAL ITEMS</h3>
                <table>
                    <tr><td class="tdspecial">Private Well</td><td class="right">$25.00</td></tr>
                    <tr><td class="tdspecial">Pool</td><td class="right">$30.00</td></tr>
                    <tr><td class="tdspecial">Hot Tub</td><td class="right">$20.00</td></tr>
                    <tr><td class="tdspecial">Crawl Space/Pilings</td><td class="right">$30.00</td></tr>
                    <tr><td class="tdspecial">Lead Paint</td><td class="right">$75.00</td></tr>
                    <tr><td class="tdspecial">Sprinkler System</td><td class="right">$25.00</td></tr>
                    <tr><td class="tdspecial">Water/Bacteria</td><td class="right">$50.00</td></tr>
                    <tr><td class="tdspecial">Water/Bacteria, nitrate & lead</td><td class="right">$125.00</td></tr>
                </table>
        
            </div>
         
            
        </div>
        <br/>
        <p>*All prices listed above are if paid at the time of inspection ONLY. There will be a $25.00 additional 
            charge for fees that are not collected on the day of inspection. Due to the distinct attributes of each 
            home, please call for a quote.
        </p>
        
    </div>
<?php
include 'footer.php';
?>