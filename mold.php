<?php
$pageTitle="Sample Reports - North Florida Home Inspection";
include 'header.php';
?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">
        
        <h1>Is Mold Sampling Necessary?</h1>
        <p>Let us help you prevent a serious problem.</p>
      </div> 
    </div>

    <div class="container well">
		<div class="row">
			<div class="col-md-6">
				<img src="images/problems/mold.png" alt="mold picture" class="img-responsive">
			</div>
			<div class="col-md-6">
			<p>Mold, like bacteria, is everywhere. In most cases it cannot be seen. When the right indoor conditions are present, mold can be hazardous to your health.</p>

			<p>EPA studies indicate that the level of indoor mold counts can be two to three times higher than outdoor levels. 
			Current lifestyles cause many people to spend as much as 90% of their time indoors.</p>
		
			<p>Poorly installed flashing and leaky roofs, unsealed stucco, leaky pipes or moisture 
			in a crawlspace can all create favorable environments for mold growth.</p>
		
			<p>Mold has the potential to cause health problems and, in severe cases, make a home uninhabitable. Allergic reactions to mold are varied and may be immediate or delayed. 
			Those suffering from allergies, asthma, compromised immune systems, children, the elderly and pregnant women are most at risk.</p>
		
			<p>If any of these conditions exist in the house or if there are health concerns, we can sample for mold at your request.</p>
			</div>
		</div>
	</div>
<?php
include 'footer.php';
?>