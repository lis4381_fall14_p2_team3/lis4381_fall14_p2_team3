<?php
$pageTitle="Who Needs An Inspection - North Florida Home Inspection";
include 'header.php';
?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">

        <h1>Who Needs An Inspection?</h1>
        <p>Are you unsure about needing an inspection?</p>
      </div>
    </div>

    <div class="container well">
        <div class="col-md-4"> 
        <p><h3>WHY INSPECT?</h3></p> 
<p>
  It doesn’t matter if the house is new or old. Mistakes can happen in construction or when repairs are made. Building materials can be flawed and will deteriorate over time. Installation techniques may not be correct. Getting an inspection can identify these issues up front, lowering your risk.
</p>
</div>
        <p>Having a professional home inspection done prior to a real estate purchase can reduce unwanted surprises on both sides.</p>  

        <p>FOR THE BUYER; identify any existing conditions that have been tolerated by or are unknown to the current home owner. This allows the buyer to be aware of any issues before purchasing the home.</p>

        <p>FOR THE SELLER; identify any conditions that could cause the loss of a sale. This gives the seller the opportunity to correct these issues before putting the home up for sale, making the house more appealing in this highly competitive market.</p>

    </div>
<?php
include 'footer.php';
?>