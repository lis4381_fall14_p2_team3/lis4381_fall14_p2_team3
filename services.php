<?php
$pageTitle="Our Services - North Florida Home Inspection";
include 'header.php';
?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">
        
        <h1>Our Services</h1>
        <p>We offer a wide range of services. We can help!</p>
      </div>
    </div>

    <div class="container well">
        
        <h2>Price Guide*</h2>
        <ul>
            <li>Up to 999 square feet..................$220.00</li>
            <li>1000-1499  square feet.................$240.00</li>
            <li>1500-1999  square feet.................$260.00</li>
            <li>2000-2499  square feet.................$280.00</li>
            <li>2500-2999  square feet.................$320.00</li>
            <li>3000-3499  square feet.................$370.00</li>
            <li>3500-3999  square feet.................$420.00</li>
            <li>4000 and above..................Call for quote</li>
            <li>Four-Point Insurance Letter............$125.00</li>
            <li>Commercial Properties...........Call for quote</li>
        </ul>
            
        <h3>CONDOMINIUMS</h3>
        <ul>
        Up to 1499 square feet ................$185.00
        1500-1999  square feet ................$210.00
        2000-2499  square feet ................$235.00
        </ul>

        ADDITIONAL ITEMS
        Private Well ...........................$25.00
        Pool ...................................$30.00
        Hot Tub.................................$20.00
        Crawl Space/Pilings ....................$30.00
        Lead Paint .............................$75.00
        Sprinkler System........................$25.00
        Water/Bacteria .........................$50.00
        Water/Bacteria, nitrate & lead ........$125.00


        
    </div>
<?php
include 'footer.php';
?>