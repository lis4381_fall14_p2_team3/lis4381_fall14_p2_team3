<?php
$pageTitle="Why NFHI? - North Florida Home Inspection";
include 'header.php';
?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="background-color: #FFEAC1;">
      <div class="container">

        <h1>Why NFHI?</h1>
        <p>See why you should chose NFHI!</p>
      </div>
    </div>



    <div class="container well">
            <div class="text-center">
            <h3>CHOOSE YOUR INSPECTOR AS CAREFULLY AS YOU CHOOSE YOUR HOME</h3>    
        </div>
        <p>
            Every house has its good and bad points. Our reports give you the positive attributes as well as the negative, providing a better picture of the house as a whole.
            Our reports are the result of a unique software developed around our professional techniques and our follow-up conversations. This combination has produced thousands of reports that anyone from a first-time buyer to the experienced buyer can understand.  
        </p>
 <br />
 
     <div class="row">
       <div class="col-md-4">
            <h2>"There is no such thing as a perfect home. But, some have fewer issues than others."</h2> 
       </div>
   <div class="col-md-4"> 
    <h3>WE PROVIDE:</h3>
        <ul>
         <li>Computerized on-site reports</li>
         <li>Digital photographs</li> 
         <li>Reports that can be e-mailed</li> 
         <li>100% Money Back Guarantee</li>
         <li>Repair and maintenance tips</li>
         <li>Follow-up consultations</li>
         <li>Discounts to repeat customers</li> 
       </ul>  
  </div>
   <div class="col-md-4">
      <h4>&#10004; HOME/TOWNHOUSE INSPECTION</h4>
  <p>  
      An inspection of all visual components
  </p>
      <h4>&#10004; CONDO INSPECTION</h4>	 
       <p>
A complete inspection, but excludes areas that may be maintained by a Homeowners Association, such as the roof, exterior, 
common areas, etc.
       </p>
<h4>&#10004; INSURANCE RELATED INSPECTIONS</h4>  
       <p>
Roof Certification, Wind Mitigation, 4 Point
       </p>
<h4>&#10004; MOLD ASSESSMENT</h4>
       <p>
Sampling of indoor/outdoor air and surfaces
       </p>
<h4>&#10004; COMERCIAL PROPERTY INSPECTION</h4> 
       <p>
A complete inspection tailored to business  related properties
</p>
</div>
    </div>
</div>
<?php
include 'footer.php';
?>